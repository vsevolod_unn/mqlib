﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using MQLib;

namespace Example
{
    class ExampleConsole
    {
        private static CancellationToken s_token;

        static void Main(string[] args)
        {
            Console.WriteLine("Введите количество воркеров");
            int WorkersCount = Convert.ToInt32(Console.ReadLine());

            var source = new CancellationTokenSource();
            s_token = source.Token;

            for (int threadId = 0; threadId < WorkersCount; threadId++)
                Task.Factory.StartNew(WorkerProc, s_token);

            Task.Factory.StartNew(CollectorProc);

            Thread.Sleep(1000);
            Task.Factory.StartNew(() => DistributorProc(WorkersCount));

            Console.ReadKey();
        }

        static void DistributorProc(object _WorkerCount)
        {
            //CDistributor dis1, dis2;
            var dis1 = new CDistributorToWorker();
            //dis1.ConfigureToWorkers();
            dis1.report = false;
            var dis2 = new CDistributorToCollector();
            //dis2.ConfigureToCollector();
            dis2.report = false;

            Thread t1 = new Thread(new ParameterizedThreadStart(ToWorkers));
            t1.Start(new object[] { dis1, _WorkerCount });

            Thread t2 = new Thread(new ParameterizedThreadStart(ToCollector));
            t2.Start(dis2);
        }

        public static void ToWorkers(object obj)
        {
            object[] objs = (object[])obj;

            CDistributorToWorker dis1 = (CDistributorToWorker)objs[0];
            int WorkerCount = (int)objs[1];

            string data;
            for (int i = 0; i < 40; i++)
            {
                data = $"Picture #{i} for worker";
                dis1.SendFrame(data);
                Thread.Sleep(30);
            }
            Thread.Sleep(5000);
            dis1.ShutDownPullers(WorkerCount);
        }

        public static void ToCollector(object obj)
        {
            CDistributorToCollector dis2 = (CDistributorToCollector)obj;
            string data;
            for (int i = 0; i < 40; i++)
            {
                data = $"Picture #{i} for collector";
                dis2.SendFrame(data);
                Thread.Sleep(30);
            }
            Thread.Sleep(15000);
            //dis2.ShutDownPullers();
        }

        static void WorkerProc()
        {
            CWorker worker = new CWorker();
            worker.report = false;

            string data = "";
            while (data != "END")
            {
                data = worker.ReceiveFrame();
                Thread.Sleep(new Random(DateTime.Now.Millisecond).Next(100, 300)); //  Simulate 'work'
                if (data != "END")
                    data += " (processed)";
                worker.SendFrame(data);
            }
        }

        static void CollectorProc()
        {
            CCollector collector = new CCollector();
            collector.report = false;

            string data = "";
            while (data != "END")
            {
                data = collector.ReceiveFrame();
            }
        }
    }
}
